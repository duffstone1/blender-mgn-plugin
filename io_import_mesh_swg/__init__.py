# Copyright 2011 Star^2 Design

# This file is part of the MTG Mesh Suite.

# MTG Mesh Suite is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# MTG Mesh Suite is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with the MTG Mesh Suite.  If not, see <http://www.gnu.org/licenses/>.

bl_info = {
    'name': "Import/Export SWG File Types (.mgn)",
    "author": "Charina (original), Duffstone (updates and new features)",
    "version": (1, 0),
    "blender": (2, 7, 9),
    "location":"File > Import > SWG Types",
    "description": "Import and Export SWG animated meshes(.mgn)",
    'category': "Import-Export",
    }
## Import modules
if "bpy" in locals():
    import imp
    imp.reload(iff_tools)
    imp.reload(mgn_tools)
    #imp.reload(mshimport)
    imp.reload(mgnimport)
    imp.reload(mgnexport)
else:
    from . import iff_tools
    from . import mgn_tools
    from . import mgnimport
    from . import mgnexport

import bpy

## function calls
def mgn_import(self, context):
    self.layout.operator(mgnimport.IMPORT_OT_galaxies_mgn.bl_idname, text="SWG Animated Meshes (.MGN)")

##def skt_import(self, context):
##    self.layout.operator(sktimport.IMPORT_OT_galaxies_skt.bl_idname, text="SWG Skeleton Files (.SKT")

def mgn_export(self, context):
    self.layout.operator(mgnexport.EXPORT_OT_galaxies_mgn.bl_idname, text="SWG Animated Meshes (.MGN)")

## register
def register():
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_import.append(mgn_import)
    bpy.types.INFO_MT_file_export.append(mgn_export)
##    bpy.types.INFO_MT_file_import.append(skt_import)

def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_import.remove(mgn_import)
    bpy.types.INFO_MT_file_export.remove(mgn_export)
##    bpy.types.INFO_MT_file_import.remove(skt_import)

if __name__ == "__main__":
    register()
